package net.netcoding.niftybukkit.minecraft;

public interface ServerPingListener {

	public void onServerPing(BukkitServer server);

}